var mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    userName: {
        type: String,
        required: true
    },
    userEmail: {
        type: String,
        required: true
    },
    region: {
        type: String,
        required: true
    },
    userPwd: {
        type: String,
        required: true
    },
    phoneNumbers: [String]


});

const user = module.exports = mongoose.model('user',userSchema);