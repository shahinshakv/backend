const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const route = require('./route')
var path = require('path');


var app = express();
app.use(express.static('./public'))

const PORT = 4050;



mongoose.connect('mongodb://localhost:27017/todolist2',{
    useMongoClient: true
});


mongoose.connection.on('connected', ()=>{
    console.log('mongodb connected')
});
mongoose.connection.on('err', (err)=>{
    console.log(err);
} )

app.use(cors());
app.use(bodyParser.json());
app.use('/',route);




app.listen(PORT, ()=>{
    console.log('server has been started on port:',PORT);

});
