const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');

const todo = require('./itemSchema');
const user = require('./user');
var app = express();

app.use(bodyParser.urlencoded({extended: false}))

router.get('/todo',(req, res, next)=>{
    todo.find((err,todo)=>{
      if (err){
          res.json(err);
      }
      else {
          res.json(todo);
          console.log("error")
      }
    });
});

router.post('/todo',(req, res, next)=>{
   let newitem = todo({
       todo: req.body.todo
   });
   newitem.save((err)=>{
       if (todo.find((req.body.todo)) == true ) {
         res.json('A user with that email has already registered. Please use a different email..');

   }
    else if (err){
         res.json(err);
     }

     else{
         res.json({msg:'items has been successfully added'});
     }


   });
});


router.put('/todo/:id',(req, res, next)=>{
  todo.findOneAndUpdate(
      {_id: req.params.id},
    {
          $set:{
              todo: req.body.todo

        }},
    (err ,result)=>{
          if (err){
              res.json(err);
          }
          else {
              res.json(result);
          }
    }
      )
});





router.delete('/todo/:id',(req, res, next)=>{

    console.log("deleted");

    todo.remove({
        _id: req.params.id
    }, (err,result)=>{
        if(err){
            res.json({msg:'the selected file already deleted'});


        }
        else {
            res.json({msg:'successfully deleted'});
        }
        })
});


//USER SIGN UP

router.get('/user',(req, res, next)=>{
    user.find((err,user)=>{
        if (err){
            res.json(err);
        }
        else {
            res.json(user);
            console.log("error")
        }
    });
});

router.get('/user/group',(req, res, next)=>{
    user.aggregate([
        {
            //$match
            //$elemMatch
            //$unwind
            //$project
            $group: {
                _id: '$region',  //$region is the column name in collection
                count: {$sum: 1}
            }
        }
    ], function (err, result) {
        if (err) {
            next(err);
        } else {
            res.json(result);
        }
    });
});

router.get('/user/count', (req, res, next)=>{
    user.count({
        region: 'kerala'
    }, function (err, result) {
        if (err) {
            next(err);
        } else {
            res.json("kerala have"+'\xa0'+result+'\xa0'+"users");
        }
    });
});

router.post('/user',(req, res, next)=>{
    let newuser = user({
        userName: req.body.userName,
        userEmail: req.body.userEmail,
        region: req.body.region,
        userPwd: req.body.userPwd,
        phoneNumbers: req.body.phoneNumbers
    });
    newuser.save((err)=>{
         if (err){
            res.json(err);
        }

        else{
            res.json({msg:'items has been successfully added'});
        }


    });
});


router.put('/user/addphone/:id',(req, res, next)=>{

    const phoneNumbers = req.body.phoneNumbers;


        user.findOneAndUpdate (
            {_id: req.params.id},
            {
                $push:{
                    phoneNumbers: phoneNumbers
                }},
                (err ,result)=> {
                    if (err) {
                        res.json(err);
                    }
                    else {
                        res.json(result);
                    }





})});

router.put('/user/editname/:id',(req,res,next)=>{
    const userName = req.body.userName;

    user.findOneAndUpdate(
        {_id: req.params.id},
        {
            $set:{ userName
        }},
        (err,result)=>{
            if(err){
                return err;
            }
            else {
                 res.json("updated successfully");

            }

    }
    )
});



router.put('/user/editemail/:id',(req,res,next)=>{
    const userEmail = req.body.userEmail;

    user.findOneAndUpdate(
    {_id: req.params.id},
    {
        $set:{ userEmail
        }},
    (err,result)=>{
        if(err)
        {
            return err;
        }
        else {
            return res.json("updated successfully");
        }
    })
});

router.put('/user/editregion/:id',(req,res,next)=>{
    const userRegion = req.body.region;

    user.findOneAndUpdate({
        _id: req.params.id
    },
        {
            $set:{userRegion}
        },
        (err,result)=>{
        if (err){
            return err
        }
        else {
            res.json("updated successfully")
        }
        }
        )
});

router.put('/user/editpwd/:id',(req,res,next)=>{
    const userPwd = req.body.userPwd;

    user.findOneAndUpdate(
        {_id: req.params.id},
        {
            $set:{userPwd}
        },
        (err,result)=>{
            if(err){
                return error;
            }
            else{
                return res.json("updated successfully");
            }
        }
        )
});

router.get('/user/cal',(req, res,next)=>{
    var first:{}
    var result=parseInt(first)+parseInt(second)

    res.json(result)
});




router.put('/user/removephone/:id',(req, res, next)=>{

    const phoneNumbers = req.body.phoneNumbers;


    user.findOneAndUpdate (
        {_id: req.params.id},
        {
            $pull:{
                phoneNumbers: phoneNumbers
            }},
        (err ,result)=> {
            if (err) {
                res.json(err);
            }
            else {
                res.json(result);
            }





        })});

  //*  user.findOneAndUpdate(
    //    {_id: req.params.id},
      //  {
        //    $set:{
//                 userName: req.body.userName,
//                 userEmail: req.body.userEmail,
//                 region: req.body.region,
//                 userPwd: req.body.userPwd,
//
//
//             }},
//         (err ,result)=>{
//             if (err){
//                 res.json(err);
//             }
//             else {
//                 res.json(result);
//             }
//         }
//     )
// });


router.delete('/user/:id',(req, res, next)=>{

    console.log("deleted");

    user.remove({
        _id: req.params.id
    }, (err,result)=>{
        if(err){
            res.json({msg:'the selected file already deleted'});


        }
        else {
            res.json({msg:'successfully deleted'});
        }
    })
});



module.exports = router;