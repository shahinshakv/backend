var mongoose = require('mongoose');

const itemSchema = mongoose.Schema({
    todo: {
        type: String,
        required: true
    }
});

const todo = module.exports = mongoose.model('todo',itemSchema);